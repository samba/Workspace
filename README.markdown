# Workspace Configuration

These are the "dotfiles", utility scripts, and other configuration I rely on in my development environment.

These were originally crafted for use on Mac OSX, predominantly, 
but will eventually updated for compatibility with Linux systems as well.

The `install.sh` script is your start point...

* Sets up the dotfiles (vim, shell, screen configuration)
* Downloads tools I use regularly (Chrome, Sublime Text, GitHub, etc.)

```shell
sh install.sh dotfiles
sh install.sh downloads
```

A shortcut also exists;

```shell
sh install.sh all
```

## Other tools

- iTerm
- PostgreSQL
- VirtualBox
- Cyberduck
- GitHub for Mac
- Sublime Text
- Hipchat
- Cinch
- Dash
- Docker
- Google Chrome

## From AppStore

- PG Commander
- Slack
- Evernote
- Skitch
- Pocket
- Kindle
- Pixelmator
- Caffeine
- Xcode
- The Unarchiver